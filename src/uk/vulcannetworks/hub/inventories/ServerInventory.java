package uk.vulcannetworks.hub.inventories;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Items;
import uk.vulcannetworks.inventories.interfaces.superclass.Menu;

public class ServerInventory extends Menu{

	public ServerInventory(String name, InventoryType inventoryType) {
		super(name, inventoryType);
	}

	@Override
	public void click(ItemStack is, Player p) {
		
		if(is.getType() == Material.BARRIER){ return; }
		
		
		List<String> lores = is.getItemMeta().getLore();
		String serverLine = "";
		for(String string : lores){
			if(string.contains("Server")){
				serverLine = string;
			}
		}
				
		String[] strings = serverLine.split("\\s+");
		bPlugin.getvAPI().sendPlayer(p, strings[1]);
			
		p.playSound(p.getLocation(), Sound.BLOCK_NOTE_PLING, 1, 1);

	}

	@Override
	public void registerItems() {
		inventory.addItem(Items.getInstance.create(Material.DIAMOND_SWORD, 1,
				ChatColor.GRAY + "Factions" +
						ChatColor.RED + "" + ChatColor.BOLD + "RED",
						ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------",
								" ",
									ChatColor.GRAY + "A Factions Experience",
										" ",
											ChatColor.GRAY + "Grow yourself with mystic powers.",
												" ",
													ChatColor.GRAY + "Hunt down those who dislike you.",
														" ",
															ChatColor.GRAY + "All on Factions" + ChatColor.RED  + "" + ChatColor.BOLD + "RED!",
																" ",
																	ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------",
																		" ",
																			ChatColor.GRAY + "Server: FactionsRed"));
		
		inventory.setItem(1, Items.getInstance.create(Material.BARRIER, 1, ChatColor.GRAY + "Empty", ChatColor.RED + "This is just a spacer, not a joinable server!"));
		inventory.addItem(Items.getInstance.create(Material.MAP, 1,
				ChatColor.GRAY + "Factions" +
						ChatColor.BLUE + "" + ChatColor.BOLD + "RPG",
						ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------",
								" ",
									ChatColor.GRAY + "A RPG Factions Experience",
										" ",
											ChatColor.GRAY + "Join with friends to destroy bosses.",
												" ",
													ChatColor.GRAY + "Hunt down those who dislike you.",
														" ",
															ChatColor.GRAY + "All on Factions" + ChatColor.BLUE  + "" + ChatColor.BOLD + "RPG!",
																" ",
																	ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------",
																		" ",
																			ChatColor.GRAY + "Server: FactionsRPG"));
		
		inventory.setItem(3, Items.getInstance.create(Material.BARRIER, 1, ChatColor.GRAY + "Empty", ChatColor.RED + "This is just a spacer, not a joinable server!"));
		inventory.addItem(Items.getInstance.create(Material.DIAMOND_PICKAXE, 1,
				ChatColor.GRAY + "Mine" +
						ChatColor.WHITE + "" + ChatColor.BOLD + "FEST",
						ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------",
								" ",
									ChatColor.GRAY + "Mining Based Survival",
										" ",
											ChatColor.GRAY + "Mine for resources to survive.",
												" ",
													ChatColor.GRAY + "Rank up through different tiers.",
														" ",
															ChatColor.GRAY + "All on Mine" + ChatColor.WHITE  + "" + ChatColor.BOLD + "FEST!",
																" ",
																	ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------",
																		" ",
																			ChatColor.GRAY + "Server: MineFest"));
	}
	
}
