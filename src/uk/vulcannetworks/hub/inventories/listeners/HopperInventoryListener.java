package uk.vulcannetworks.hub.inventories.listeners;

import org.bukkit.Sound;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import uk.vulcannetworks.bukkit.bPlugin;

public class HopperInventoryListener implements Listener {

	//TODO: Find more efficient way to do this.
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){		
		if((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)){
			if((e.getItem() != null) && (e.getItem().getItemMeta().getDisplayName() != null)){
				if(e.getItem().getItemMeta().getDisplayName().contains("Server Selector")){
					e.getPlayer().openInventory(bPlugin.getMenuManager().getMenu("Server").inventory);
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_NOTE_PLING, 1, 1);
					e.setUseInteractedBlock(Result.DENY);
					e.setUseItemInHand(Result.DENY);
					e.setCancelled(true);
				}
				
				if(e.getItem().getItemMeta().getDisplayName().contains("Particles Selector")){
					e.getPlayer().openInventory(bPlugin.getMenuManager().getMenu("Particle").inventory);
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_NOTE_PLING, 1, 1);
					e.setUseInteractedBlock(Result.DENY);
					e.setUseItemInHand(Result.DENY);
					e.setCancelled(true);
				}
			}
		}
	}
	
}
