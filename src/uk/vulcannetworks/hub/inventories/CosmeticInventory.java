package uk.vulcannetworks.hub.inventories;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import uk.vulcannetworks.hub.bukkit.BukkitPlugin;
import uk.vulcannetworks.hub.utils.Items;
import uk.vulcannetworks.inventories.interfaces.superclass.Menu;

public class CosmeticInventory extends Menu{

	public ArrayList<String> fire = new ArrayList<String>();
	public ArrayList<String> explosion1 = new ArrayList<String>();
	public ArrayList<String> explosion2 = new ArrayList<String>();
	public ArrayList<String> ender = new ArrayList<String>();
	public ArrayList<String> firework = new ArrayList<String>();
	
	public CosmeticInventory(String name, InventoryType inventoryType) {
		super(name, inventoryType);
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPlugin.getInstance, new Runnable(){
			@SuppressWarnings("deprecation")
			public void run(){
				try{
				for(String string : fire){
					Player player = (Player) Bukkit.getPlayer(string);
					if((player == null) || !(player.isOnline())){
						fire.remove(string);
						return;
					}
					Bukkit.getPlayer(string).getWorld().playEffect(player.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
				}
				
				for(String string : explosion1){
					Player player = (Player) Bukkit.getPlayer(string);
					if((player == null) || !(player.isOnline())){
						explosion1.remove(string);
						return;
					}
					Bukkit.getPlayer(string).getWorld().playEffect(player.getLocation(), Effect.EXPLOSION, 1);
				}
				
				for(String string : explosion2){
					Player player = (Player) Bukkit.getPlayer(string);
					if((player == null) || !(player.isOnline())){
						explosion2.remove(string);
						return;
					}
					Bukkit.getPlayer(string).getWorld().playEffect(player.getLocation(), Effect.EXPLOSION_LARGE, 1);
				}
				
				for(String string : ender){
					Player player = (Player) Bukkit.getPlayer(string);
					if((player == null) || !(player.isOnline())){
						ender.remove(string);
						return;
					}
					Bukkit.getPlayer(string).getWorld().playEffect(player.getLocation(), Effect.ENDER_SIGNAL, 1);
				}
				
				for(String string : firework){
					Player player = (Player) Bukkit.getPlayer(string);
					if((player == null) || !(player.isOnline())){
						firework.remove(string);
						return;
					}
					Bukkit.getPlayer(string).getWorld().playEffect(player.getLocation(), Effect.FIREWORKS_SPARK, 1);
				}
				}catch (Exception e) {
					return;
				}
			}
		}, 10, 10);
		
	}

	@Override
	public void click(ItemStack is, Player p) {
		String c = is.getItemMeta().getDisplayName();
		
		//TODO: Remove from other particles.
		//TODO: Find more efficient way todo this.
		
		fire.remove(p.getName());
		firework.remove(p.getName());
		explosion1.remove(p.getName());
		explosion2.remove(p.getName());
		ender.remove(p.getName());
		
		if(c.contains("Firework")){
			firework.add(p.getName());
			return;
		}
		
		if(c.contains("Fire")){
			fire.add(p.getName());
			return;
		}
		
		if(c.contains("Explosion (x1)")){
			explosion1.add(p.getName());
			return;
		}
		
		if(c.contains("Explosion (x2")){
			explosion2.add(p.getName());
			return;
		}
		
		if(c.contains("Ender")){
			ender.add(p.getName());
			return;
		}
		
		return;
		
	}

	@Override
	public void registerItems() {
		inventory.addItem(Items.getInstance.create(Material.MOB_SPAWNER, 1, ChatColor.RED + "Fire Particles", ""));
		inventory.addItem(Items.getInstance.create(Material.TNT, 1, ChatColor.RED + "Explosion (x1) Particles", ""));
		inventory.addItem(Items.getInstance.create(Material.TNT, 2, ChatColor.RED + "Explosion (x2) Particles", ""));
		inventory.addItem(Items.getInstance.create(Material.ENDER_PEARL, 1, ChatColor.RED + "Ender Particles", ""));
		inventory.addItem(Items.getInstance.create(Material.FIREWORK, 1, ChatColor.RED + "Firework Particles", ""));
	}

}
