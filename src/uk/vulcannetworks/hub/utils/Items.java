package uk.vulcannetworks.hub.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Items {

	public static Items getInstance = new Items();
	
	public ItemStack create(Material material, int amount, String displayName, String...lores){
		ItemStack iStack = new ItemStack(material, amount);
		ItemMeta iMeta = iStack.getItemMeta();
		iMeta.setDisplayName(displayName);
		ArrayList<String> lore = new ArrayList<>();
		for(String string : lores){
			lore.add(string);
		}
		iMeta.setLore(lore);
		iStack.setItemMeta(iMeta);
		return iStack;
	}
	
	public static void setup(){
		getInstance = new Items();
	}
	
 }
