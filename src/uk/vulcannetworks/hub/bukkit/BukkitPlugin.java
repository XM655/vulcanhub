package uk.vulcannetworks.hub.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.plugin.java.JavaPlugin;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.bukkit.general.JoinListener;
import uk.vulcannetworks.hub.bukkit.general.protection.BlockProtection;
import uk.vulcannetworks.hub.inventories.CosmeticInventory;
import uk.vulcannetworks.hub.inventories.ServerInventory;
import uk.vulcannetworks.hub.inventories.listeners.HopperInventoryListener;
import uk.vulcannetworks.hub.utils.Items;

public class BukkitPlugin extends JavaPlugin{	
	public org.bukkit.plugin.PluginManager pm = Bukkit.getPluginManager();
	
	public static BukkitPlugin getInstance;
	
	@Override
	public void onEnable(){
		getInstance = this;
		setupInventories();
		setupListeners();
		initialize();
	}
	
	public void setupInventories(){
		Items.setup();
		bPlugin.getMenuManager().registerMenu("Server", new ServerInventory(ChatColor.GRAY + "Server Selector", InventoryType.HOPPER));
		bPlugin.getMenuManager().registerMenu("Particle", new CosmeticInventory(ChatColor.GRAY + "Particle Selector", InventoryType.HOPPER));
	}
	
	public void setupListeners(){
		/** Inventories **/
		pm.registerEvents(new HopperInventoryListener(), this);
		
		/** General Purpose Listeners **/
		pm.registerEvents(new JoinListener(), this);
		
		/** Protection Listeners **/
		pm.registerEvents(new BlockProtection(), this);
	}
		
	public void initialize(){
		getInstance = this;
	}
}
