package uk.vulcannetworks.hub.bukkit.general;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Items;

public class JoinListener implements Listener{

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		
		Player player = (Player) e.getPlayer();
		
		player.teleport(bPlugin.getvAPI().getSpawn());
		
		e.setJoinMessage(null);
		player.playSound(player.getLocation(), Sound.BLOCK_NOTE_PLING, 1, 1);
		player.getInventory().clear();
		
		player.getInventory().setItem(1, Items.getInstance.create(Material.EYE_OF_ENDER, 1, ChatColor.RED + ">> " + ChatColor.GRAY + "Server Selector" + ChatColor.RED + " <<" ,
				ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------",
					" ",
						ChatColor.GRAY + "Your portal to the VulcanNetwork",
							" ",
								ChatColor.GRAY + "Right click this item whilst holding it.",
									" ",
										ChatColor.GRAY + "Experience new experiences, check this inventory monthly.",
													" ",
														ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------"));
		
	   player.getInventory().setItem(7, Items.getInstance.create(Material.BLAZE_POWDER, 1, ChatColor.RED + ">> " + ChatColor.GRAY + "Particles Selector" + ChatColor.RED + " <<" ,
				ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------",
					" ",
						ChatColor.GRAY + "VulcanNetworks Hub Particles",
							" ",
								ChatColor.GRAY + "Right click this item whilst holding it.",
									" ",
										ChatColor.GRAY + "For VIP Users",
													" ",
														ChatColor.LIGHT_PURPLE + "" + ChatColor.STRIKETHROUGH + "--------------------------------------"));
		
		player.sendTitle(ChatColor.GRAY + "VulcanNetworks", ChatColor.GRAY + "Play.VulcanNetworks.uk");
		
	}
	
	
}
