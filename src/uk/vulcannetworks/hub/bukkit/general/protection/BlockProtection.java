package uk.vulcannetworks.hub.bukkit.general.protection;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import uk.vulcannetworks.bukkit.bPlugin;

public class BlockProtection implements Listener {

	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		if(e.getPlayer().hasPermission("vulcannetworks.admin")){ bPlugin.getvAPI().sendAb(e.getPlayer(), ChatColor.GRAY + "[Admin] Block breaking and placing enabled."); return; }
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		if(e.getPlayer().hasPermission("vulcannetworks.admin")){ bPlugin.getvAPI().sendAb(e.getPlayer(), ChatColor.GRAY + "[Admin] Block breaking and placing enabled."); return; }
		
		e.setCancelled(true);
	}
	
}
